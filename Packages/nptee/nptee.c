//
//	nptee --- Quick hack for non-blocking read from STDIN
//		and write to multiple output named pipes.
//

static char *version = 
"	COPYRIGHT (c) 2019, David McGough, KB4FXC, 11/20/2019\n"
"	All rights reserved. Version 0.3.\n"
"\n"
" Redistribution and use in source and binary forms, with or without\n"
" modification, are permitted provided that the following conditions are met:\n"
"\n"
" 1. Redistributions of source code must retain the above copyright notice, this\n"
"    list of conditions and the following disclaimer.\n"
" 2. Redistributions in binary form must reproduce the above copyright notice,\n"
"    this list of conditions and the following disclaimer in the documentation\n"
"    and/or other materials provided with the distribution.\n"
"\n"
" THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS \"AS IS\" AND\n"
" ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED\n"
" WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE\n"
" DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR\n"
" ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES\n"
" (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;\n"
" LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND\n"
" ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT\n"
" (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS\n"
" SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.\n"
"\n"
" The views and conclusions contained in the software and documentation are those\n"
" of the authors and should not be interpreted as representing official policies,\n"
" either expressed or implied.\n"
"\n";
// 
//
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pthread.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <signal.h>


// 20mSec frames are 320 bytes each.
#define BUFSIZE 340

// 60 seconds of 20mS PCM frames 
#define FIFO_SIZE 60*50

#define THREAD_COUNT 10
#define mS(x)	(x * 1000)

typedef struct {
	uint8_t	buf[BUFSIZE];
	uint16_t count;
} BUF;

typedef struct {
	pthread_t pt;
	char	*fifoname;
	int num;
	volatile int head;
} LIST;

LIST thread_list[THREAD_COUNT];
BUF fifo[FIFO_SIZE];
volatile int tail;
volatile int threads_running = 1;
static int debug = 0;


void *writer_thread(void *args) {

	int fdout, head;
	LIST *info = args;

	if (debug) printf ("Thread #%d, head = %d, fifoname = %s\n",
			info->num, info->head, info->fifoname);

	unlink (info->fifoname);
	umask (0);
	if (mkfifo(info->fifoname, 0644));

	while ((fdout = open(info->fifoname, O_WRONLY|O_NONBLOCK)) < 0) {
		if (debug) printf ("Thread #%d, waiting for listener! fifoname = %s\n",
				info->num, info->fifoname);
		if (!threads_running)
			return NULL;
		sleep(1);
	}

	head = info->head;
	while (threads_running) {
		ssize_t c;

		if (head == tail) {
			if (debug) printf ("Thread #%d, nothing to do. sleeping. fifoname = %s\n",
					info->num, info->fifoname);
			usleep(mS(500));
			continue;
		}
		while(((c = write(fdout, fifo[head].buf, fifo[head].count)) == -1) && errno == EAGAIN && threads_running)
			usleep(mS(300));

		if (c  == -1) {		// Client probably went away...Buffer and wait.
			usleep(mS(200));
			continue;
		}

		if (c != fifo[head].count)
			printf ("Thread #%d, write underrun: %d written, %d requested! fifoname = %s\n",
					info->num, c, fifo[head].count, info->fifoname);

		if (++head >= FIFO_SIZE) head = 0;
	}

	return (NULL);
}

int main(int argc, char *argv[])
{
	{ // Disable signal processing....
		sigset_t mask;
		sigfillset(&mask);
		sigprocmask(SIG_SETMASK, &mask, NULL);
	}

	if (argc < 2 || argc - 1 > THREAD_COUNT) {
		fprintf(stderr, "\nUsage: %s named_pipe [...named_pipe]\n", argv[0]);
		fprintf(stderr, "\n\n%s\n\n", version);
		exit(-1);
	}

	system ("/bin/rm -rf /tmp/outsound");
	mkdir("/tmp/outsound", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

	tail = 0;
	for (int i = 0; i < argc - 1; i++) {
		char tmp[512];

		thread_list[i].num = i;
		thread_list[i].head = 0;
		snprintf (tmp, sizeof(tmp) - 1, "/tmp/outsound/%s", argv[i+1]);
		thread_list[i].fifoname = strdup(tmp);
		if(pthread_create(&thread_list[i].pt, NULL, writer_thread, &thread_list[i])) {
			fprintf(stderr, "Unable to create thread, exiting!\n");
			goto bailout;
		}
	}

	for (int i;;) {
		if ((i = read(0, fifo[tail].buf, BUFSIZE)) <= 0)
			goto bailout; 		// STDIN closed or error, bail out!

		fifo[tail].count = i;
		if (debug) printf ("Read count = %d\n", i);

		// Increment tail pointer, make sure the value of tail is never out of bounds!
		i = tail + 1;
		tail = (i >= FIFO_SIZE) ? 0 : i;
	}

bailout:
	printf ("bail out\n");
	threads_running = 0;
	for (int i = 0; i < argc - 1; i++)
		pthread_join(thread_list[i].pt, NULL);

	exit(0);
}

